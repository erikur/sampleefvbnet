﻿Imports System.Data.SqlClient
Imports System.Transactions

Module Module1

    Sub Main()
        Dim db As New SampleDapperDbEntities

        'Dim result = From p In db.Pengguna
        '             Join a In db.Artikel
        '             On p.UserId Equals a.UserId
        '             Select p.UserId, p.Username, p.Nama, a.Judul, a.Isi, a.Tanggal

        'For Each ar In result
        '    Console.WriteLine($"Username: {ar.Username} Judul: {ar.Judul}")
        'Next

        'Dim result = From va In db.ViewArtikelWithPengguna
        '             Select va

        'For Each va In result
        '    Console.WriteLine($"UserId:{va.UserId} Username:{va.Username} Judul:{va.Judul}")
        'Next


        'Dim result = From a In db.Artikel.Include("Pengguna")
        '             Order By a.Judul Ascending
        '             Select a

        Dim result = db.Artikel.SqlQuery("select * from Artikel inner join Pengguna 
                                          on Artikel.UserId=Pengguna.UserId").ToList()

        For Each a In result
            Console.WriteLine($"Judul: {a.Judul} Isi: {a.Isi} Pengguna:{a.Pengguna.Nama}")
        Next

        'tambah data pengguna
        'Try
        '    Dim newPengguna As New Pengguna With {
        '        .UserId = "22334455", .Username = "Joko", .Nama = "Joko Purwadi",
        '        .Aturan = "Admin"}
        '    db.Pengguna.Add(newPengguna)

        '    Dim newArtikel As New Artikel With {
        '        .Judul = "Belajar EF", .Isi = "Belajar EF", .UserId = newPengguna.UserId}
        '    db.Artikel.Add(newArtikel)

        '    db.SaveChanges()
        '    Console.WriteLine("Data Berhasil Ditambah")
        'Catch ex As Exception
        '    Console.WriteLine("Data Gagal Ditambah")
        'End Try


        'harus ditambahkan System.Transactions
        'Using trans As New TransactionScope



        '    trans.Complete()
        'End Using

        'https://github.com/StackExchange/Dapper
    End Sub

End Module
